import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Image, Icon, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

// rename finishEdit to CloseEditor
const EditPost = ({
  post,
  updatePost,
  uploadImage,
  finishEdit
}) => {
  const { id, body: postBody, image: postImage } = post;
  const [body, setBody] = useState(postBody);
  const [image, setImage] = useState(postImage);
  const [isUploading, setIsUploading] = useState(false);
  const [imageButtonContent, setImageButtonContent] = useState(
    image?.id ? 'Change image' : 'Attach image'
  );

  const handleEditPost = async () => {
    if (!body) {
      return;
    }
    await updatePost({ id, imageId: image?.id, body });
    finishEdit();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ id: imageId, link: imageLink });
      setImageButtonContent('Update image');
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      <Form onSubmit={() => handleEditPost()}>
        <Form.TextArea
          rows={8}
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.id && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.link} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          {imageButtonContent}
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button.Group floated="right">
          <Button color="red" onClick={() => finishEdit()}>Cancel</Button>
          <Button color="green" type="submit">Save</Button>
        </Button.Group>
      </Form>
    </Segment>
  );
};

EditPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  finishEdit: PropTypes.func.isRequired
};

export default EditPost;
