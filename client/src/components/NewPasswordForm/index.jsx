import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment } from 'semantic-ui-react';

const NewPasswordForm = ({ changePassword, token, email }) => {
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(true);

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const handleChangePasswordClick = async () => {
    const isValid = isPasswordValid;
    if (!isValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await changePassword({ token, email, password });
    } catch {
      // TODO: show error
      setIsLoading(false);
    }
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleChangePasswordClick}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={email}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          error={!isPasswordValid}
          onChange={ev => passwordChanged(ev.target.value)}
          onBlur={() => setIsPasswordValid(Boolean(password))}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Reset
        </Button>
      </Segment>
    </Form>
  );
};

NewPasswordForm.propTypes = {
  changePassword: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
  email: PropTypes.string
};

NewPasswordForm.defaultProps = {
  email: ''
};

export default NewPasswordForm;
