import React from 'react';
import { List, Image } from 'semantic-ui-react';
import { getUserImgLink } from 'src/helpers/imageHelper';

const ReactionUsersList = users => {
  if (!users) {
    return null;
  }
  // const andMoreListItem = Number(reactionsCount) > users.length
  //   ? (
  //     <List.Item key="last-item">
  //       and more
  //     </List.Item>
  //   ) : null;
  return (
    <List>
      {users.map(user => (
        <List.Item key={user.id}>
          <Image avatar src={getUserImgLink(user.user.image)} />
          <List.Content>
            {user.user.username}
          </List.Content>
        </List.Item>
      ))}
      {/* {andMoreListItem} */}
    </List>
  );
};

export default ReactionUsersList;
