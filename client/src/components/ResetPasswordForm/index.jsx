import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Segment, Popup } from 'semantic-ui-react';

const ResetPasswordForm = ({ reset, resetResponse }) => {
  const [email, setEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [showMessage, setShowMessage] = useState(false);
  const [message, setMessage] = useState('');
  const [success, setSuccess] = useState(false);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleSubmitClick = async () => {
    const isValid = isEmailValid;
    if (!isValid || isLoading) {
      return;
    }
    setIsLoading(true);

    try {
      setShowMessage(false);
      await reset({ email });
      setIsLoading(false);
      setMessage(resetResponse.message);
      setSuccess(true);
      setShowMessage(true);
    } catch (err) {
      setEmail(email);
      setShowMessage(true);
      setSuccess(false);
      setMessage(err.message);
      setIsLoading(false);
    }
  };

  const formInput = (
    <Form.Input
      fluid
      icon="at"
      iconPosition="left"
      placeholder="Email"
      type="email"
      error={!isEmailValid}
      onChange={ev => emailChanged(ev.target.value)}
      onBlur={() => setIsEmailValid(validator.isEmail(email))}
    />
  );

  return (
    <Form name="loginForm" size="large" onSubmit={handleSubmitClick}>
      <Segment>
        <Popup
          open={showMessage}
          trigger={formInput}
          disabled={!showMessage}
          content={message || resetResponse?.message}
          position="left center"
        />
        {success
          ? (
            <Button type="button" color="teal" icon="check" fluid size="large" primary />
          ) : (
            <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
              Reset
            </Button>
          )}
      </Segment>
    </Form>
  );
};

ResetPasswordForm.propTypes = {
  reset: PropTypes.func.isRequired,
  resetResponse: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ResetPasswordForm;
