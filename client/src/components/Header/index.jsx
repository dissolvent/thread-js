import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import UserStatus from 'src/components/UserStatus';
import { Header as HeaderUI, Image, Grid, Icon, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Header = ({ user, logout, updateStatus }) => (
  <div className={styles.headerWrp}>
    <Grid centered container columns="2">
      <Grid.Column>
        {user && (
          <>
            <NavLink exact to="/">
              <HeaderUI className={styles.headerUserInfo}>
                <Image circular src={getUserImgLink(user.image)} />
              </HeaderUI>
            </NavLink>
            <HeaderUI className={styles.headerUserInfo}>
              <HeaderUI.Content as="h4">
                {user.username}
                <HeaderUI.Subheader>
                  <UserStatus
                    className={styles.headerUserStatus}
                    userStatus={user.status}
                    update={updateStatus}
                  />
                </HeaderUI.Subheader>
              </HeaderUI.Content>
            </HeaderUI>
          </>
        )}
      </Grid.Column>
      <Grid.Column textAlign="right">
        <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
          <Icon name="user circle" size="large" />
        </NavLink>
        <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
          <Icon name="log out" size="large" />
        </Button>
      </Grid.Column>
    </Grid>
  </div>
);

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateStatus: PropTypes.func.isRequired
};

export default Header;
