import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, Grid, Icon } from 'semantic-ui-react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';

import styles from './styles.module.scss';

const ChangeUserImage = ({
  toggle,
  setNewAvatar
}) => {
  const [uploadedImg, setUploadedImg] = useState(null);
  const [completedCrop, setCompletedCrop] = useState(null);
  const [isUploading, setIsUploading] = useState(false);
  const [crop, setCrop] = useState({
    unit: 'px',
    width: 50,
    aspect: 1 / 1
  });

  const handleSelectImage = ({ target }) => {
    setIsUploading(true);
    URL.revokeObjectURL(uploadedImg);
    const blobUrl = URL.createObjectURL(target.files[0]);
    setUploadedImg(blobUrl);
    setIsUploading(false);
  };

  const handleCloseEditor = () => {
    URL.revokeObjectURL(uploadedImg);
    toggle();
  };

  const uploadCropedImg = (imageElement, cropSelection) => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    const scaleX = imageElement.naturalWidth / imageElement.width;
    const scaleY = imageElement.naturalHeight / imageElement.height;
    canvas.width = crop.width;
    canvas.height = crop.height;

    ctx.drawImage(
      imageElement,
      cropSelection.x * scaleX,
      cropSelection.y * scaleY,
      cropSelection.width * scaleX,
      cropSelection.height * scaleY,
      0,
      0,
      cropSelection.width,
      cropSelection.height
    );
    new Promise(resolve => {
      canvas.toBlob(blob => resolve(blob), 'image/jpeg', 1);
    }).then(blob => {
      setIsUploading(true);
      setNewAvatar(blob);
    }).then(handleCloseEditor);
  };

  return (
    <Modal dimmer="blurring" centered size="small" open onClose={() => handleCloseEditor()}>
      <Modal.Content>
        <Grid>
          <Grid.Row centered>
            {uploadedImg === null
              ? (<h2>Select photo</h2>)
              : (
                <ReactCrop
                  src={uploadedImg}
                  className={styles.cropImage}
                  maxWidth="200"
                  keepSelection
                  ruleOfThirds
                  circularCrop
                  crop={crop}
                  onChange={c => setCrop(c)}
                  onComplete={c => setCompletedCrop(c)}
                />
              )}
          </Grid.Row>
          <Grid.Row columns={2}>
            <Grid.Column>
              <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                <Icon name="image" />
                Upload photo
                <input name="image" type="file" accept="image/*" onChange={handleSelectImage} hidden />
              </Button>
            </Grid.Column>
            <Grid.Column>
              <Button.Group floated="right">
                <Button color="red" onClick={() => handleCloseEditor()}>Cancel</Button>
                (
                <Button
                  color="green"
                  // have to grab rendered image, to properly calculate img size
                  onClick={() => uploadCropedImg(
                    document.querySelector(`.${styles.cropImage} .ReactCrop__image`),
                    completedCrop
                  )}
                  disabled={!completedCrop?.width || !completedCrop?.height}
                  loading={isUploading}
                  content="Save"
                />
                )
              </Button.Group>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Modal.Content>
    </Modal>
  );
};

ChangeUserImage.propTypes = {
  toggle: PropTypes.func.isRequired,
  setNewAvatar: PropTypes.func.isRequired
};

export default ChangeUserImage;
