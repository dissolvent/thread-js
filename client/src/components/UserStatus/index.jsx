import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from 'semantic-ui-react';

const StatusField = ({ userStatus, update, className }) => {
  const [status, setStatus] = useState(userStatus);
  const [editStatus, setEditStatus] = useState(false);

  const handleUpdateStatus = async newStatus => {
    await update(newStatus);
    setEditStatus(false);
    setStatus(userStatus);
  };

  const handleEnterEditMode = () => {
    setStatus(userStatus);
    setEditStatus(true);
  };

  return (
    <Input
      action={
        editStatus
          ? (
            <Button
              basic
              type="submit"
              icon="check"
              color="green"
              onClick={() => handleUpdateStatus(status)}
            />
          ) : (
            <Button
              basic
              type="submit"
              icon="edit"
              onClick={handleEnterEditMode}
            />
          )
      }
      className={className}
      placeholder="Tell everyone something"
      disabled={!editStatus}
      type="text"
      maxLength="30"
      value={editStatus ? status : userStatus}
      onChange={e => setStatus(e.target.value)}
    />
  );
};

StatusField.propTypes = {
  userStatus: PropTypes.string,
  update: PropTypes.func.isRequired,
  className: PropTypes.string
};

StatusField.defaultProps = {
  userStatus: '',
  className: ''
};

export default StatusField;
