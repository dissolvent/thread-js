import React from 'react';
import { Popup, Label, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import ReactionUsersList from 'src/components/ReactionUsersList';

import styles from './styles.module.scss';

const ReactionButton = ({
  postId,
  reactionCount,
  likeOrDislike,
  users,
  isLike,
  highlightStyle,
  loadUsers
}) => {
  const thumbsDirection = isLike ? 'thumbs up' : 'thumbs down';

  const reactionButton = (
    <Label
      basic
      size="small"
      as="a"
      className={styles.toolbarBtn}
      onClick={async () => {
        await likeOrDislike(postId);
        await loadUsers({ id: postId, isLike });
        await loadUsers({ id: postId, isLike: !isLike });
      }}
      onMouseEnter={async () => {
        if (Number(reactionCount) && !users) {
          await loadUsers({ id: postId, isLike });
        }
      }}
    >
      <Icon name={thumbsDirection} className={`${highlightStyle}`} />
      {reactionCount}
    </Label>
  );
  return (
    <Popup
      on="hover"
      trigger={reactionButton}
      content={ReactionUsersList(users)}
      className={styles.popup}
      mouseEnterDelay={700}
      hideOnScroll
      disabled={Number(reactionCount) === 0}
      offset="35px, 0"
      basic
    />
  );
};

ReactionButton.propTypes = {
  postId: PropTypes.string.isRequired,
  reactionCount: PropTypes.string.isRequired,
  likeOrDislike: PropTypes.func.isRequired,
  isLike: PropTypes.bool.isRequired,
  highlightStyle: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.any),
  loadUsers: PropTypes.func.isRequired
};

ReactionButton.defaultProps = {
  users: undefined
};

export default ReactionButton;
