import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import ReactionButton from './ReactionButton';

import styles from './styles.module.scss';

const Post = (
  { post,
    canMakeChanges,
    likePost,
    dislikePost,
    getReactionUsers,
    toggleExpandedPost,
    sharePost,
    editPost,
    deletePost }
) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    updatedAt,
    isLike,
    likeUsers,
    dislikeUsers
  } = post;

  const creationDate = moment(createdAt).fromNow();
  const updateDate = moment(updatedAt).isAfter(moment(createdAt))
    ? moment(updatedAt).fromNow()
    : undefined;

  const editButton = canMakeChanges
    ? (
      <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => editPost(post)}>
        <Icon name="edit" />
      </Label>
    )
    : null;

  const deleteButton = canMakeChanges
    ? (
      <Label basic size="small" as="a" className={styles.deleteBtn} onClick={() => deletePost(id)}>
        <Icon name="delete" />
      </Label>
    )
    : null;

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {creationDate}
            {updateDate && ` - changed ${updateDate}`}
          </span>
          { deleteButton }
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <ReactionButton
          postId={id}
          reactionCount={likeCount}
          likeOrDislike={likePost}
          users={likeUsers}
          isLike
          highlightStyle={isLike === true ? styles.liked : ''}
          loadUsers={getReactionUsers}
        />
        <ReactionButton
          postId={id}
          reactionCount={dislikeCount}
          likeOrDislike={dislikePost}
          users={dislikeUsers}
          isLike={false}
          highlightStyle={isLike === false ? styles.disliked : ''}
          loadUsers={getReactionUsers}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        { editButton }
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  canMakeChanges: PropTypes.bool.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  getReactionUsers: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default Post;
