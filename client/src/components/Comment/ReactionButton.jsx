import React from 'react';
import { Popup, Comment, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import ReactionUsersList from 'src/components/ReactionUsersList';

import styles from './styles.module.scss';

const ReactionButton = ({
  postId,
  commentId,
  reactionCount,
  likeOrDislike,
  users,
  isLike,
  loadUsers
}) => {
  const thumbsDirection = isLike ? 'thumbs up' : 'thumbs down';
  const reactionButton = (
    <Comment.Action
      as="a"
      className={styles.toolbarBtn}
      onClick={async () => {
        await likeOrDislike(commentId);
        await loadUsers({ id: commentId, isLike, postId });
        await loadUsers({ id: commentId, isLike: !isLike, postId });
      }}
      onMouseEnter={async () => {
        if (Number(reactionCount) && !users) {
          await loadUsers({ id: commentId, isLike, postId });
        }
      }}
    >
      <Icon name={thumbsDirection} />
      {reactionCount}
    </Comment.Action>
  );
  return (
    <Popup
      on="hover"
      trigger={reactionButton}
      content={ReactionUsersList(users)}
      className={styles.commentPopup}
      mouseEnterDelay={700}
      hideOnScroll
      disabled={Number(reactionCount) === 0}
      offset="30px, 0"
      basic
    />
  );
};

ReactionButton.propTypes = {
  postId: PropTypes.string.isRequired,
  commentId: PropTypes.string.isRequired,
  reactionCount: PropTypes.string.isRequired,
  likeOrDislike: PropTypes.func.isRequired,
  isLike: PropTypes.bool.isRequired,
  users: PropTypes.arrayOf(PropTypes.any),
  loadUsers: PropTypes.func.isRequired
};

ReactionButton.defaultProps = {
  users: undefined
};

export default ReactionButton;
