import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Form, Button } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import ReactionButton from './ReactionButton';

import styles from './styles.module.scss';

const Comment = (
  { comment: {
    id,
    body,
    createdAt,
    updatedAt,
    user,
    likeCount,
    dislikeCount,
    likeUsers,
    dislikeUsers,
    postId
  },
  like,
  dislike,
  canMakeChanges,
  getReactionUsers,
  deleteComment,
  editComment }
) => {
  const [commentBody, setCommentBody] = useState(body);
  const [inEditMode, setInEditMode] = useState(false);
  const commentWereChanged = moment(updatedAt).isAfter(moment(createdAt))
    ? moment(updatedAt).fromNow()
    : null;

  const handleApplyChanges = async () => {
    if (!commentBody.trim()) {
      return;
    }
    await editComment({ id, body: commentBody });
    setInEditMode(!inEditMode);
  };

  const handleCancelEdit = () => {
    setInEditMode(!inEditMode);
    setCommentBody(body);
  };

  const editButton = canMakeChanges
    ? (
      <CommentUI.Action as="a" onClick={() => setInEditMode(!inEditMode)}>
        <Icon name="edit" />
      </CommentUI.Action>
    )
    : null;

  const deleteButton = canMakeChanges
    ? (
      <CommentUI.Action as="a" onClick={() => deleteComment(id)}>
        <Icon name="delete" />
      </CommentUI.Action>
    )
    : null;

  const likeButton = (
    <ReactionButton
      postId={postId}
      commentId={id}
      reactionCount={likeCount}
      likeOrDislike={like}
      isLike
      users={likeUsers}
      loadUsers={getReactionUsers}
    />
  );

  const dislikeButton = (
    <ReactionButton
      postId={postId}
      commentId={id}
      reactionCount={dislikeCount}
      likeOrDislike={dislike}
      isLike={false}
      users={dislikeUsers}
      loadUsers={getReactionUsers}
    />
  );

  const readComponent = (
    <>
      <CommentUI.Text>
        {commentBody}
      </CommentUI.Text>
      <CommentUI.Actions>
        {likeButton}
        {dislikeButton}
        {editButton}
        {deleteButton}
      </CommentUI.Actions>
    </>
  );

  const editComponent = (
    <Form reply onSubmit={() => handleApplyChanges()}>
      <Form.TextArea
        value={commentBody}
        placeholder="Type a comment..."
        onChange={ev => setCommentBody(ev.target.value)}
      />
      <Button.Group floated="right">
        <Button color="red" onClick={() => handleCancelEdit()}>Cancel</Button>
        <Button color="green" type="submit">Save</Button>
      </Button.Group>
    </Form>
  );

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {`posted ${moment(createdAt).fromNow()}`}
          {commentWereChanged && ' - changed'}
          {user.status && ` - ${user.status}`}
        </CommentUI.Metadata>
        { inEditMode ? editComponent : readComponent }
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  like: PropTypes.func.isRequired,
  dislike: PropTypes.func.isRequired,
  getReactionUsers: PropTypes.func.isRequired,
  canMakeChanges: PropTypes.bool.isRequired,
  editComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired
};

export default Comment;
