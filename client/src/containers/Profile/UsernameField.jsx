import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Input, Button, Popup } from 'semantic-ui-react';

import styles from './styles.module.scss';

const UsernameField = ({
  username,
  updateProfileName
}) => {
  const [name, setName] = useState(username);
  const [inputIsEnabled, setInputIsEnabled] = useState(false);
  const [showError, setShowError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const handleCancelUsernameEdit = () => {
    setInputIsEnabled(false);
    setShowError(false);
    setErrorMessage('');
    setName(username);
  };

  const handleUpdateUsername = async newName => {
    setShowError(false);
    setErrorMessage('');

    if (newName.trim() === username) {
      setName(username);
      setInputIsEnabled(false);
      return;
    }

    try {
      await updateProfileName(newName);
      setInputIsEnabled(false);
    } catch (err) {
      setShowError(true);
      setErrorMessage(err.message);
      setInputIsEnabled(true);
    }
  };

  const inputField = (
    <Input
      icon="user"
      iconPosition="left"
      className={styles.profileInfoFields}
      action={
        inputIsEnabled
          ? (
            <>
              <Button type="submit" icon="cancel" color="red" onClick={handleCancelUsernameEdit} />
              <Button type="submit" icon="check" color="green" onClick={() => handleUpdateUsername(name)} />
            </>
          ) : (
            <Button type="submit" icon="edit" onClick={() => setInputIsEnabled(true)} />
          )
      }
      onChange={e => setName(e.target.value)}
      placeholder="Username"
      type="text"
      error={showError}
      disabled={!inputIsEnabled}
      value={name}
    />
  );

  return (
    <Popup
      open={showError}
      trigger={inputField}
      content={errorMessage}
      position="left center"
      disabled={!showError}
    />
  );
};

UsernameField.propTypes = {
  username: PropTypes.string.isRequired,
  updateProfileName: PropTypes.func.isRequired
};

export default UsernameField;
