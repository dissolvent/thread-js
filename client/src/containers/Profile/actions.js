import * as authService from 'src/services/authService';
import * as userService from 'src/services/userService';
import {
  SET_USER,
  EDIT_PROFILE_IMAGE,
  RESET_PASSWORD
} from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setEditProfileImageAction = image => dispatch => dispatch({
  type: EDIT_PROFILE_IMAGE,
  image
});

const setResetPassword = reset => dispatch => dispatch({
  type: RESET_PASSWORD,
  reset
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleRequestResetResponse = authResponsePromise => async dispatch => {
  const response = await authResponsePromise;
  dispatch(setResetPassword(response));
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const requestResetPassword = email => handleRequestResetResponse(authService.requestReset(email));

export const getResetEmail = token => handleRequestResetResponse(authService.getReset(token));

export const applyNewPassword = request => handleAuthResponse(authService.applyReset(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const toggleImageEdit = user => async dispatch => {
  dispatch(setEditProfileImageAction(user));
};

export const setNewAvatar = image => async dispatch => {
  const updated = await userService.uploadAvatar(image);
  dispatch(setUser(updated));
};

export const updateProfileName = payload => async (dispatch, getRootState) => {
  const { profile: { user } } = getRootState();
  const updatedProfile = await userService.updateProfile({ id: user.id, username: payload });
  dispatch(setUser({ ...user, ...updatedProfile }));
};

export const updateUserStatus = request => async (dispatch, getRootState) => {
  const { profile: { user } } = getRootState();
  const { status } = await userService.updateProfile({ id: user.id, status: request });
  dispatch(setUser({ ...user, status }));
};
