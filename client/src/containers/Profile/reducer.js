import { SET_USER, EDIT_PROFILE_IMAGE, RESET_PASSWORD } from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false
      };
    case EDIT_PROFILE_IMAGE:
      return {
        ...state,
        editProfileImage: action.image
      };
    case RESET_PASSWORD:
      return {
        ...state,
        resetResponse: action.reset
      };
    default:
      return state;
  }
};
