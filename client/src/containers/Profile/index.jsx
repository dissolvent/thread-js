import React from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Grid,
  Image,
  Input,
  Label
} from 'semantic-ui-react';
import ChangeUserImage from 'src/components/ChangeUserImage';
import UserStatus from 'src/components/UserStatus';
import UsernameField from './UsernameField';

import {
  toggleImageEdit,
  setNewAvatar,
  updateProfileName,
  updateUserStatus
} from './actions';

import styles from './styles.module.scss';

const Profile = ({
  user,
  editProfileImage,
  updateProfileName: updateProfileNameAction,
  updateUserStatus: updateUserStatusAction,
  setNewAvatar: setNewAvatarAction,
  toggleImageEdit: toggleImageEditAction
}) => (
  <Grid centered style={{ paddingTop: 30 }}>
    <Grid.Column textAlign="center">
      <Grid.Row>
        <div className="ui image">
          <Image size="medium" src={getUserImgLink(user.image)} circular />
          <Label
            icon="camera"
            className={styles.changeAvatarBtn}
            as="a"
            size="big"
            circular
            onClick={() => toggleImageEditAction(user)}
          />
        </div>
      </Grid.Row>
      <Grid.Row>
        <br />
        <UsernameField username={user.username} updateProfileName={updateProfileNameAction} />
        <br />
        <Input
          icon="at"
          className={styles.profileInfoFields}
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <UserStatus
          className={styles.profileInfoFields}
          userStatus={user.status}
          update={updateUserStatusAction}
        />
      </Grid.Row>
    </Grid.Column>
    {editProfileImage
    && (
      <ChangeUserImage
        toggle={toggleImageEditAction}
        setNewAvatar={setNewAvatarAction}
      />
    )}
  </Grid>
);

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  editProfileImage: PropTypes.objectOf(PropTypes.any),
  updateProfileName: PropTypes.func.isRequired,
  updateUserStatus: PropTypes.func.isRequired,
  setNewAvatar: PropTypes.func.isRequired,
  toggleImageEdit: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {},
  editProfileImage: undefined
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user,
  editProfileImage: rootState.profile.editProfileImage
});

const actions = {
  toggleImageEdit,
  setNewAvatar,
  updateProfileName,
  updateUserStatus
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
