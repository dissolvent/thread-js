import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  EDIT_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const editPostAction = post => ({
  type: EDIT_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

// still quiеt don't like this solution
export const likePost = postId => async (dispatch, getRootState) => {
  const { isLike } = await postService.likePost(postId);
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : { ...post, isLike }));
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction({ ...expandedPost, isLike }));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { isLike } = await postService.dislikePost(postId);
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : { ...post, isLike }));
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction({ ...expandedPost, isLike }));
  }
};

export const getPostReactionUsers = filter => async (dispatch, getRootState) => {
  const { id: postId, isLike } = filter;
  const { totalReactions, users } = await postService.getPostReactionUsers(filter);
  const { posts: { posts, expandedPost } } = getRootState();

  const reactionTypeUsers = isLike ? 'likeUsers' : 'dislikeUsers';
  const reactionTypeCount = isLike ? 'likeCount' : 'dislikeCount';
  // map reaction users to post and update likes count to avoid difference
  const updated = posts.map(post => (post.id !== postId
    ? post
    : { ...post,
      [reactionTypeUsers]: users,
      [reactionTypeCount]: String(totalReactions) }));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction({
      ...expandedPost,
      [reactionTypeUsers]: users,
      [reactionTypeCount]: String(totalReactions)
    }));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updateComment = request => async (dispatch, getRootState) => {
  const updatedComment = await commentService.updateComment(request);

  const { posts: { expandedPost } } = getRootState();
  const { comments } = expandedPost;

  const updatedComments = comments.map(
    comment => (comment.id !== updatedComment.id ? comment : { ...comment, ...updatedComment })
  );

  if (expandedPost && expandedPost.id === updatedComment.postId) {
    dispatch(setExpandedPostAction({ ...expandedPost, comments: updatedComments }));
  }
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  await commentService.deleteComment(commentId);

  const updateCommentsCount = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const { comments } = expandedPost;
  const removedComment = comments.filter(comment => comment.id === commentId)[0];

  const updatedPosts = posts.map(post => (post.id !== removedComment.postId
    ? post
    : updateCommentsCount(post)));

  dispatch(setPostsAction(updatedPosts));

  if (expandedPost && expandedPost.id === removedComment.postId) {
    const remaningComments = comments.filter(comment => comment.id !== commentId);
    dispatch(setExpandedPostAction({ ...updateCommentsCount(expandedPost), comments: remaningComments }));
  }
};

// Still struggle with post and comment likes to come up with the better solution
// current is ugly - with "show user who like this" it's 4 requests
const updateCommentLikesHandler = commentId => async (dispatch, getRootState) => {
  const updatedComment = await commentService.getComment(commentId);
  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === updatedComment.postId) {
    const { comments } = expandedPost;
    const updated = comments.map(
      comment => (comment.id !== updatedComment.id ? comment : { ...comment, ...updatedComment })
    );
    dispatch(setExpandedPostAction({ ...expandedPost, comments: updated }));
  }
};

export const likeComment = commentId => async dispatch => {
  await commentService.likeComment(commentId);
  dispatch(updateCommentLikesHandler(commentId));
};

export const dislikeComment = commentId => async dispatch => {
  await commentService.dislikeComment(commentId);
  dispatch(updateCommentLikesHandler(commentId));
};

export const getCommentReactionUsers = filter => async (dispatch, getRootState) => {
  const { id: commentId, postId, isLike } = filter;
  const { totalReactions, users } = await commentService.getCommentReactionUsers({ id: commentId, isLike });
  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === postId && users.length) {
    const reactionTypeUsers = isLike ? 'likeUsers' : 'dislikeUsers';
    const reactionTypeCount = isLike ? 'likeCount' : 'dislikeCount';
    const updatedComments = expandedPost.comments.map(comment => (
      comment.id !== commentId ? comment
        : { ...comment,
          [reactionTypeUsers]: users,
          [reactionTypeCount]: String(totalReactions)
        }
    ));

    dispatch(setExpandedPostAction({
      ...expandedPost,
      comments: updatedComments
    }));
  }
};

export const togglePostEditor = post => async dispatch => {
  dispatch(editPostAction(post));
};

// check server side, why return full post?
export const updatePost = request => async (dispatch, getRootState) => {
  const { comments, ...updatedPost } = await postService.updatePost(request);
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== updatedPost.id ? post : updatedPost));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === updatedPost.id) {
    dispatch(setExpandedPostAction({ ...updatedPost, comments }));
  }
};

export const deletePost = postId => async (dispatch, getRootState) => {
  await postService.deletePost(postId);
  const { posts: { posts, expandedPost } } = getRootState();
  const remainingPosts = posts.filter(post => post.id !== postId);

  dispatch(setPostsAction(remainingPosts));

  // close expanded post if it deleted
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(undefined));
  }
};
