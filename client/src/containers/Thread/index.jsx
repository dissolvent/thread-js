import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import PostEditor from 'src/containers/PostEditor';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  getPostReactionUsers,
  toggleExpandedPost,
  addPost,
  togglePostEditor,
  updatePost,
  deletePost
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10,
  excludeUserId: undefined
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  inEditModePost,
  hasMorePosts,
  addPost: createPost,
  likePost: likePostAction,
  dislikePost: dislikePostAction,
  updatePost: updatePostAction,
  getPostReactionUsers: getPostReactionUsersAction,
  togglePostEditor: togglePostEditorAction,
  toggleExpandedPost: toggleExpandedPostAction,
  deletePost: deletePostAction
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    setHideOwnPosts(false);
    postsFilter.excludeUserId = undefined;
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toogleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    setShowOwnPosts(false);
    postsFilter.userId = undefined;
    postsFilter.excludeUserId = hideOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const deletePosthandler = id => {
    deletePostAction(id);
    postsFilter.from -= 1;
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          toggle
          label="Hide my posts"
          checked={hideOwnPosts}
          onChange={toogleHideOwnPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={likePostAction}
            dislikePost={dislikePostAction}
            getReactionUsers={getPostReactionUsersAction}
            toggleExpandedPost={toggleExpandedPostAction}
            sharePost={sharePost}
            canMakeChanges={userId === post.userId}
            editPost={togglePostEditorAction}
            deletePost={deletePosthandler}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost
      && (
        <ExpandedPost
          userId={userId}
          sharePost={sharePost}
          editPost={togglePostEditorAction}
          deletePost={deletePosthandler}
        />
      )}
      {sharedPostId
      && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
      {inEditModePost
      && (
        <PostEditor
          updatePost={updatePostAction}
          uploadImage={uploadImage}
          toggle={togglePostEditorAction}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  inEditModePost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  getPostReactionUsers: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  togglePostEditor: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  inEditModePost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  inEditModePost: rootState.posts.editingPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  getPostReactionUsers,
  toggleExpandedPost,
  togglePostEditor,
  addPost,
  updatePost,
  deletePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
