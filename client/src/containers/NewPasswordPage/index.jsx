import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { applyNewPassword, getResetEmail } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header } from 'semantic-ui-react';
import NewPasswordForm from 'src/components/NewPasswordForm';

const NewPasswordPage = ({
  match,
  resetResponse,
  applyNewPassword: applyPass,
  getResetEmail: getEmail
}) => {
  useEffect(() => {
    if (!resetResponse?.email) {
      getEmail(match.params.token);
    }
  });

  const { email } = resetResponse;
  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Enter new password
        </Header>
        <NewPasswordForm changePassword={applyPass} token={match.params.token} email={email} />
      </Grid.Column>
    </Grid>
  );
};

NewPasswordPage.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  resetResponse: PropTypes.objectOf(PropTypes.any),
  applyNewPassword: PropTypes.func.isRequired,
  getResetEmail: PropTypes.func.isRequired
};

NewPasswordPage.defaultProps = {
  resetResponse: {}
};

const actions = { applyNewPassword, getResetEmail };

const mapStateToProps = rootState => ({
  resetResponse: rootState.profile.resetResponse
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewPasswordPage);
