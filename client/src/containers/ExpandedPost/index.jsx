import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost,
  dislikePost,
  getPostReactionUsers,
  getCommentReactionUsers,
  toggleExpandedPost,
  addComment,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment } from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  userId,
  post,
  sharePost,
  editPost,
  deletePost,
  likePost: likePostAction,
  dislikePost: dislikePostAction,
  getPostReactionUsers: getPostReactionUsersAction,
  toggleExpandedPost: toggleExpandedPostAction,
  addComment: addCommentAction,
  updateComment: editCommentAction,
  deleteComment: deleteCommentAction,
  likeComment: likeCommentAction,
  dislikeComment: dislikeCommentAction,
  getCommentReactionUsers: getCommentReactionUsersAction
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggleExpandedPostAction()}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            likePost={likePostAction}
            dislikePost={dislikePostAction}
            getReactionUsers={getPostReactionUsersAction}
            toggleExpandedPost={toggleExpandedPostAction}
            canMakeChanges={userId === post.userId}
            editPost={editPost}
            sharePost={sharePost}
            deletePost={deletePost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                <Comment
                  key={comment.id}
                  id={comment.id}
                  comment={comment}
                  like={likeCommentAction}
                  dislike={dislikeCommentAction}
                  canMakeChanges={userId === comment.userId}
                  getReactionUsers={getCommentReactionUsersAction}
                  editComment={editCommentAction}
                  deleteComment={deleteCommentAction}
                />
              ))}
            <AddComment postId={post.id} addComment={addCommentAction} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ExpandedPost.propTypes = {
  userId: PropTypes.string,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  getPostReactionUsers: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  getCommentReactionUsers: PropTypes.func.isRequired
};

ExpandedPost.defaultProps = {
  userId: undefined
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = { likePost,
  dislikePost,
  getPostReactionUsers,
  toggleExpandedPost,
  addComment,
  likeComment,
  dislikeComment,
  updateComment,
  deleteComment,
  getCommentReactionUsers
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
