import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { requestResetPassword } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import ResetPasswordForm from 'src/components/ResetPasswordForm';

const ResetPasswordPage = ({ requestResetPassword: resetPass, resetResponse }) => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Forgot Password
      </Header>
      <ResetPasswordForm reset={resetPass} resetResponse={resetResponse} />
      <Message>
        Suddenly remembered?
        {' '}
        <NavLink exact to="/login">Sign In</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

ResetPasswordPage.propTypes = {
  requestResetPassword: PropTypes.func.isRequired,
  resetResponse: PropTypes.objectOf(PropTypes.any)
};

ResetPasswordPage.defaultProps = {
  resetResponse: {}
};

const actions = { requestResetPassword };

const mapStateToProps = rootState => ({
  resetResponse: rootState.profile.resetResponse
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResetPasswordPage);
