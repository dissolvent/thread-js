import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal } from 'semantic-ui-react';
import { togglePostEditor } from 'src/containers/Thread/actions';
import EditPost from 'src/components/EditPost';

const PostEditor = ({
  post,
  updatePost,
  uploadImage,
  togglePostEditor: toggle
}) => (
  <Modal dimmer="blurring" centered open onClose={() => toggle()}>
    <EditPost
      post={post}
      updatePost={updatePost}
      uploadImage={uploadImage}
      finishEdit={toggle}
    />
  </Modal>
);

PostEditor.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  togglePostEditor: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.editingPost
});

const actions = { togglePostEditor };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostEditor);
