import callWebApi from 'src/helpers/webApiHelper';

export const uploadAvatar = async image => {
  const response = await callWebApi({
    endpoint: '/api/users/avatar',
    type: 'PUT',
    attachment: image
  });
  return response.json();
};

export const updateProfile = async request => {
  const response = await callWebApi({
    endpoint: 'api/users/',
    type: 'PUT',
    request
  });
  return response.json();
};
