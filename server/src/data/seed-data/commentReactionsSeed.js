const now = new Date();

export default new Array(15)
  .fill(true)
  .concat(new Array(15).fill(false))
  .map(isLike => ({
    isLike,
    createdAt: now,
    updatedAt: now
  }));
