/* eslint-disable no-console */
import { usersSeed, userImagesSeed } from '../seed-data/usersSeed';
import { postsSeed, postImagesSeed } from '../seed-data/postsSeed';
import commentsSeed from '../seed-data/commentsSeed';
import commentReactionsSeed from '../seed-data/commentReactionsSeed';
import postReactionsSeed from '../seed-data/postReactionsSeed';

const randomIndex = length => Math.floor(Math.random() * length);
const mapLinks = images => images.map(x => `'${x.link}'`).join(',');

export default {
  up: async (queryInterface, Sequelize) => {
    try {
      const options = {
        type: Sequelize.QueryTypes.SELECT
      };

      // Add images.
      await queryInterface.bulkInsert('images', userImagesSeed.concat(postImagesSeed), {});

      const userImagesQuery = `SELECT id FROM "images" WHERE link IN (${mapLinks(userImagesSeed)});`;
      const userImages = await queryInterface.sequelize.query(userImagesQuery, options);

      const postImagesQuery = `SELECT id FROM "images" WHERE link IN (${mapLinks(postImagesSeed)});`;
      const postImages = await queryInterface.sequelize.query(postImagesQuery, options);

      // Add users.
      const usersMappedSeed = usersSeed.map((user, i) => ({
        ...user,
        imageId: userImages[i] ? userImages[i].id : null
      }));
      await queryInterface.bulkInsert('users', usersMappedSeed, {});
      const users = await queryInterface.sequelize.query('SELECT id FROM "users";', options);

      // Add posts.
      const postsMappedSeed = postsSeed.map((post, i) => ({
        ...post,
        userId: users[randomIndex(users.length)].id,
        imageId: postImages[i] ? postImages[i].id : null
      }));
      await queryInterface.bulkInsert('posts', postsMappedSeed, {});
      const posts = await queryInterface.sequelize.query('SELECT id FROM "posts";', options);

      // Add comments.
      const commentsMappedSeed = commentsSeed.map(comment => ({
        ...comment,
        userId: users[randomIndex(users.length)].id,
        postId: posts[randomIndex(posts.length)].id
      }));
      await queryInterface.bulkInsert('comments', commentsMappedSeed, {});
      const comments = await queryInterface.sequelize.query('SELECT id FROM "comments";', options);

      // Add post reactions.
      // get bad shuffled array of [userId, postId], each pair unique,
      // to avoid user reaction duplicates
      const mapEveryUserIdToEveryPostId = users.flatMap(
        user => posts.map(post => [user.id, post.id])
      ).sort(() => Math.random() - 0.5);

      const postReactionsMappedSeed = mapEveryUserIdToEveryPostId.map(userPostPair => {
        const [userId, postId] = userPostPair;
        const reaction = postReactionsSeed[randomIndex(postReactionsSeed.length)];
        return { ...reaction, userId, postId };
      });
      await queryInterface.bulkInsert('postReactions', postReactionsMappedSeed, {});

      // Add comment reactions.
      // get poorly shuffled array of [userId, commentId], each pair unique,
      // to avoid user reaction duplicates
      const mapEveryUserIdToEveryCommentId = users.flatMap(
        user => comments.map(comment => [user.id, comment.id])
      ).sort(() => Math.random() - 0.5);
      const commentReactionsMappedSeed = mapEveryUserIdToEveryCommentId.map(userCommentPair => {
        const [userId, commentId] = userCommentPair;
        const reaction = commentReactionsSeed[randomIndex(commentReactionsSeed.length)];
        return { ...reaction, userId, commentId };
      });
      await queryInterface.bulkInsert('commentReactions', commentReactionsMappedSeed, {});
    } catch (err) {
      console.log(`Seeding error: ${err}`);
    }
  },

  down: async queryInterface => {
    try {
      await queryInterface.bulkDelete('postReactions', null, {});
      await queryInterface.bulkDelete('comments', null, {});
      await queryInterface.bulkDelete('posts', null, {});
      await queryInterface.bulkDelete('users', null, {});
      await queryInterface.bulkDelete('images', null, {});
      await queryInterface.bulkDelete('commentReactions', null, {});
    } catch (err) {
      console.log(`Seeding error: ${err}`);
    }
  }
};
