import { Op } from 'sequelize';
import sequelize from '../db/connection';
import { PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
const commentLikesQuery = bool => (`(SELECT COUNT(*)
  FROM "commentReactions" as "reactions"
  WHERE "comments"."id" = "reactions"."commentId"
  AND "reactions"."isLike" = ${bool})`
);

const loggedUserPostLikes = userId => (`
(SELECT "postReactions"."isLike"
FROM "postReactions"
WHERE "postReactions"."userId" = '${userId}'
  AND "postReactions"."postId" = "post"."id")
`);

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      excludeUserId,
      loggedUserId
    } = filter;
    const where = {};
    if (userId) {
      Object.assign(where, { userId });
    }
    if (excludeUserId) {
      Object.assign(where, {
        userId: {
          [Op.not]: excludeUserId
        }
      });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE
                          "post"."id" = "comment"."postId"
                          AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
          [sequelize.literal(loggedUserPostLikes(loggedUserId)), 'isLike']
        ],
        exclude: ['deletedAt']
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  // yeah, just notice that adding something to getById is bad idea. 21.06.2020
  getPostById(filter) {
    const { id, loggedUserId } = filter;

    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
          [sequelize.literal(loggedUserPostLikes(loggedUserId)), 'isLike']
        ],
        exclude: ['deletedAt']
      },
      include: [{
        model: CommentModel,
        attributes: {
          include: [
            [sequelize.literal(commentLikesQuery(true)), 'likeCount'],
            [sequelize.literal(commentLikesQuery(false)), 'dislikeCount']
          ],
          exclude: ['deletedAt']
        },
        include: [{
          model: UserModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }]
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }
}

export default new PostRepository(PostModel);
