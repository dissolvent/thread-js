import sequelize from 'sequelize';
import { CommentReactionModel, CommentModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class CommentReactionRepository extends BaseRepository {
  getUserReaction(userId, commentId) {
    return this.model.findOne({
      group: [
        'commentReaction.id',
        'comment.id'
      ],
      where: { userId, commentId },
      include: [{
        model: CommentModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  async getCommentReactions(filter) {
    const {
      id: commentId,
      isLike,
      limit = 20
    } = filter;
    const users = await this.model.findAll({
      attributes: {
        include: [
          [sequelize.literal(`(SELECT COUNT(*)
          FROM "commentReactions" as "reactions"
          WHERE "reactions"."isLike" = '${isLike}'
            AND "reactions"."commentId" = '${commentId}')`), 'reactionsCount']
        ]
      },
      where: { commentId, isLike },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }],
      group: [
        'commentReaction.id',
        'user.id',
        'user->image.id'
      ],
      order: [['updatedAt', 'ASC']],
      limit
    });
    const count = await this.model.count(
      {
        where: { commentId, isLike }
      }
    );

    return { totalReactions: count, users };
  }
}

export default new CommentReactionRepository(CommentReactionModel);
