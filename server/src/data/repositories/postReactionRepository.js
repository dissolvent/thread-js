import { PostReactionModel, PostModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class PostReactionRepository extends BaseRepository {
  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: [
        'postReaction.id',
        'post.id'
      ],
      where: { userId, postId },
      include: [{
        model: PostModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  async getPostReactionUsers(filter) {
    const {
      limit,
      id: postId,
      isLike
    } = filter;
    const users = await this.model.findAll({
      attributes: {
        exclude: ['userId']
      },
      where: { postId, isLike },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }],
      order: [['updatedAt', 'ASC']],
      limit
    });
    const count = await this.model.count(
      {
        where: { postId, isLike }
      }
    );

    return { totalReactions: count, users };
  }
}

export default new PostReactionRepository(PostReactionModel);
