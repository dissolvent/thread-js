import { Router } from 'express';
import * as authService from '../services/authService';
import * as userService from '../services/userService';
import authenticationMiddleware from '../middlewares/authenticationMiddleware';
import registrationMiddleware from '../middlewares/registrationMiddleware';
import jwtMiddleware from '../middlewares/jwtMiddleware';

const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .post('/login', authenticationMiddleware, (req, res, next) => authService.login(req.user)
    .then(data => res.send(data))
    .catch(next))
  .post('/register', registrationMiddleware, (req, res, next) => authService.register(req.user)
    .then(data => res.send(data))
    .catch(next))
  .get('/reset', (req, res, next) => userService.getUserByToken(req.query.token)
    .then(email => res.send(email))
    .catch(next))
  .post('/reset', (req, res, next) => {
    // TODO: research ways of retrieving user host when proxy is used
    const { headers: { 'x-forwarded-host': clientHost } } = req;
    return authService.resetPassword(clientHost, req.body)
      .then(data => res.send(data))
      .catch(next);
  })
  .put('/reset', (req, res, next) => authService.applyNewPassword(req.body)
    .then(data => res.send(data))
    .catch(next))
  .get('/user', jwtMiddleware, (req, res, next) => userService.getUserById(req.user.id)
    .then(data => res.send(data))
    .catch(next));

export default router;
