import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/react', (req, res, next) => commentService.getCommentReactions({ ...req.query })
    .then(reactions => res.send(reactions))
    .catch(next))
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => res.send(reaction))
    .catch(next))
  .put('/:id', (req, res, next) => commentService.updateComment(req.user.id, { id: req.params.id, body: req.body })
    .then(comment => (res.send(comment)))
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteComment(req.user.id, req.params.id)
    .then(comment => res.send(comment))
    .catch(next));

export default router;
