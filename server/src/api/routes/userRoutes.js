import { Router } from 'express';
import * as userService from '../services/userService';
import * as imageService from '../services/imageService';
import imageMiddleware from '../middlewares/imageMiddleware';

const router = Router();

router
  .put('/avatar', imageMiddleware, (req, res, next) => (imageService.upload(req.file)
    .then(image => userService.updateUserAvatar(req.user.id, image))
    .then(updated => res.send(updated))
    .catch(next)))
  .put('/', (req, res, next) => userService.updateProfile(req.user.id, req.body)
    .then(response => res.send(response))
    .catch(next));
export default router;
