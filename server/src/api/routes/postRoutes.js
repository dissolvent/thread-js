import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts({ ...req.query, loggedUserId: req.user.id })
    .then(posts => res.send(posts))
    .catch(next))
  .get('/react', (req, res, next) => postService.getPostReactionUsers({ ...req.query })
    .then(reaction => res.send(reaction))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById({ loggedUserId: req.user.id, id: req.params.id })
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(
    req.headers['x-forwarded-host'], req.user.id, req.body
  )
    .then(reaction => {
      // notify a user if someone (not himself) liked or disliked his post
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        if (reaction.dataValues.isLike) {
          req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
        } else {
          req.io.to(reaction.post.userId).emit('dislike', 'Your post was disliked');
        }
      }
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', (req, res, next) => postService.updatePost(req.user.id, { postId: req.params.id, payload: req.body })
    .then(post => (res.send(post)))
    .catch(next))
  .delete('/:id', (req, res, next) => postService.deletePost(req.user.id, req.params.id)
    .then(post => res.send(post))
    .catch(next));

export default router;
