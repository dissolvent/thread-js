import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const getCommentReactions = ({ id, isLike, limit = 20 }) => (
  commentReactionRepository.getCommentReactions({ id, isLike, limit })
);

export const updateComment = async (userId, { id, body }) => {
  const { userId: creatorId } = await commentRepository.getCommentById(id);

  if (userId !== creatorId) {
    throw new Error('You can edit only your own comments');
  }
  const updatedComment = await commentRepository.updateById(id, body);

  return updatedComment || {};
};

export const deleteComment = async (userId, id) => {
  const { userId: creatorId } = await commentRepository.getCommentById(id);

  if (userId !== creatorId) {
    throw new Error('You can delete only your comment');
  }

  await commentRepository.deleteById(id);
  // deleteById return 1 if succefull, 0 if not
  // what should return if something went wrong?
  return {};
};

export const setReaction = async (userId, { commentId, isLike = true }) => {
  // callback to use as promise
  const updateOrDelete = react => (react.isLike === isLike
    ? commentReactionRepository.deleteById(react.id)
    : commentReactionRepository.updateById(react.id, { isLike }));

  const reaction = await commentReactionRepository.getUserReaction(userId, commentId);
  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike });

  return Number.isInteger(result) ? {} : commentReactionRepository.getUserReaction(userId, commentId);
};
