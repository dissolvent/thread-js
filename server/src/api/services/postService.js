import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import userRepository from '../../data/repositories/userRepository';
import { postWasLikedEmail } from '../../helpers/emailHelper';

const handleLikeEmail = async (sendToUserId, whoLikedId, postId, clientHost) => {
  const { email } = await userRepository.getUserById(sendToUserId);
  const { username } = await userRepository.getUserById(whoLikedId);
  postWasLikedEmail(username, email, postId, clientHost);
};

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = filter => postRepository.getPostById(filter);

export const getPostReactionUsers = ({ id, isLike, limit = 20 }) => (
  postReactionRepository.getPostReactionUsers({ id, isLike, limit })
);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const setReaction = async (clientHost, userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // send email to user if his post liked
  if (result.isLike === true) {
    const { user: { id } } = await postRepository.getPostById({ id: postId, loggedUserId: userId });
    handleLikeEmail(id, userId, postId, clientHost);
  }
  // the result is an integer when an entity is deleted - No 1 when delted and 0 when not
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const updatePost = async (userId, { postId, payload }) => {
  const { userId: creatorId } = await postRepository.getPostById(
    { id: postId, loggedUserId: userId }
  );

  if (creatorId !== userId) {
    throw new Error('You can edit only your own posts');
  }

  const { imageId, body } = payload;
  await postRepository.updateById(postId, { imageId, body });
  return postRepository.getPostById({ id: postId, loggedUserId: userId });
};

export const deletePost = async (userId, postId) => {
  const post = await postRepository.getPostById({ id: postId, loggedUserId: userId });
  if (userId !== post.userId) {
    throw new Error('You can delete only your own posts');
  }
  const deletedPost = await postRepository.deleteById(postId);
  // the result is an integer when an entity is deleted
  return Number.isInteger(deletedPost) ? { success: true } : { success: false };
};
