import crypto from 'crypto';
import { createToken } from '../../helpers/tokenHelper';
import { encrypt } from '../../helpers/cryptoHelper';
import { resetPasswordEmail } from '../../helpers/emailHelper';
import userRepository from '../../data/repositories/userRepository';

export const login = async ({ id }) => ({
  token: createToken({ id }),
  user: await userRepository.getUserById(id)
});

export const register = async ({ password, ...userData }) => {
  const newUser = await userRepository.addUser({
    ...userData,
    password: await encrypt(password)
  });
  return login(newUser);
};

export const resetPassword = async (host, request) => {
  const { email } = request;
  const user = await userRepository.getByEmail(email);

  if (!user) {
    throw Error('No account with that email address exists.');
  }

  const token = crypto.randomBytes(20).toString('hex');
  await userRepository.updateById(user.id, { resetPasswordToken: token });

  await resetPasswordEmail(token, user, host);

  return {
    success: true,
    message: 'Email with link to reset password were send. Please check your box'
  };
};

// TODO: add verification token-email?
// TODO: force user to login manually after password change?
/* eslint-disable-next-line */
export const applyNewPassword = async ({ token, password, email }) => {
  const { id } = await userRepository.getByToken(token);
  try {
    const user = await userRepository.updateById(id,
      { password: await encrypt(password),
        resetPasswordToken: null });
    return login(user);
  } catch (err) {
    return err;
  }
};
