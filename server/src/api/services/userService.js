import userRepository from '../../data/repositories/userRepository';

const updateUsername = async (id, name) => {
  const trimmedName = name.trim();
  if (!trimmedName.length) {
    throw new Error('Name not provided');
  }
  const alreadyExists = await userRepository.getByUsername(trimmedName);
  if (alreadyExists) {
    throw new Error('Username already exists');
  }
  const { username, updatedAt } = await userRepository.updateById(id, { username: trimmedName });
  return { username, updatedAt };
};

const updateUserStatus = async (id, toUpdate) => {
  const { status } = await userRepository.updateById(id, { status: toUpdate });
  return { status };
};

export const getUserById = async userId => {
  const { id, username, email, imageId, image, status } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image, status };
};

export const getUserByToken = async token => {
  const { email } = await userRepository.getByToken(token);
  return { email };
};

export const updateUserAvatar = async (userId, image) => {
  const { id } = image;
  await userRepository.updateById(userId, { imageId: id });
  return getUserById(userId);
};

export const updateProfile = async (id, payload) => {
  if (Object.prototype.hasOwnProperty.call(payload, 'username')) {
    return updateUsername(id, payload.username);
  }
  if ((Object.prototype.hasOwnProperty.call(payload, 'status'))) {
    return updateUserStatus(id, payload.status);
  }
  return {};
};
