import sgMail from '@sendgrid/mail';
import { sendgridApiKey, senderEmail } from '../config/sendgridConfig';

sgMail.setApiKey(sendgridApiKey);

/* eslint-disable */
const resetText = (host, token) => `<p>You are receiving this because you (or someone else) have requested the reset of the password for your Thread JS account.</p>
<p>Please follow the link to reset password</p>
<a href="http://${host}/reset/${token}">Reset password</a>
<p>If you did not request this, please ignore this email and your password will remain unchanged</p>`;

const postLiked = (host, username, postId) => `<p>Hello</p><p>Your post was liked by ${username}.</p> <p>Post: <a href="http://${host}/share/${postId}">link</a>.</p>`;
/* eslint-enable */

const sendMsg = async msg => {
  try {
    await sgMail.send(msg);
  } catch (error) {
    // TODO handle
    /* eslint-disable */
    console.error(error);
    if (error.response) {
      // TODO handle
      console.error(error.response.body);
    }
    /* eslint-enable */
  }
};

export const resetPasswordEmail = (token, user, clientHost) => {
  const msg = {
    from: senderEmail,
    to: user.email,
    subject: 'ThreadJS password reset',
    html: resetText(clientHost, token)
  };
  sendMsg(msg);
};

export const postWasLikedEmail = (username, email, postId, clientHost) => {
  const msg = {
    from: senderEmail,
    to: email,
    subject: 'ThreadJS your post was liked',
    html: postLiked(clientHost, username, postId)
  };
  sendMsg(msg);
};
